<?php
/**
 * Created by PhpStorm.
 * User: CGIT
 * Date: 2/14/2018
 * Time: 3:03 PM
 */

class Laptop
{
    public $processore = "core i7";
    protected $color = "Blue";

    public function getProcessore(){
        return $this->processore;
    }

    public function setProcessore($y){
        $this->processore = $y;
    }

    public function getColor(){
        return $this->color;
    }

    public function setColor($x){
        $this->color = $x;
    }

}

